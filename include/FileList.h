/**
 * @file FileList.h - Program to print all the non-directory files in the local
 * directory in descending order of file
 * size.
 */
//
// Created by lukecjcurrie on 5/7/20.
//

#pragma once

#ifdef _WIN32

#include <win-dirent.h>

#else

#include <dirent.h>

#endif

#include <limits.h>
#include <stdint.h>

/**
 * @brief Struct to hold information about non-directory files.
 *
 * Used to keep each filename associated with its respective size.
 */
typedef struct {
  char name[NAME_MAX + 1]; /**< The name of the file */
  off_t fileSize;          /**< The size of the file in bytes */
} FileInfo;

/// Convenience typedef to avoid need for struct keyword
typedef struct dirent dirent;

/**
 * @brief Parse the arguments from the command line
 * @param[in] argc The number of arguments passed
 * @param[in] argv The passed-in arguments
 * @param[out] workingDir The directory whose files are to be listed.
 * @param[out] pathStr A C-string representation of the path to the directory.
 *
 */
void get_dir_path(int argc, char const *const *argv, DIR **workingDir,
                  char pathStr[PATH_MAX + 1]);

/**
 * @brief Open the local directory. Directory must be closed when no longer
 * used.
 *
 * If the directory fails to open, exit the program.
 *
 * @return A pointer to the local directory
 */
DIR *open_directory(char const pathStr[PATH_MAX + 1]);

/**
 * @brief Count the number of non-directory files in the given directory.
 *
 * Rewind the directory pointer to the beginning of the directory
 *
 * @param[in] workingDir A pointer to the directory whose files to count
 * @return The number of non-directory files in the given directory
 */
size_t count_files(DIR *workingDir);

/**
 * @brief Allocate an array of FileInfo.
 *
 * Exit the program if allocation fails.
 *
 * Memory must be freed once no longer needed.
 * @param[in] fileCount The size of the array in terms of FileInfos.
 * @return A pointer to the allocated FileInfo array.
 */
FileInfo *allocate_file_info_array(size_t fileCount);

/**
 * @brief Iterate through the given directory and load relative file info into
 * the given FileInfo array.
 *
 * @param[in] fileCount The size of the FileInfo info in FileInfos
 * @param[in] workingDir A pointer ot the directory to load file info from.
 * @param[out] filenameList A pre-allocated array of FileInfo structs to be
 * filled.
 * @param[in] pathStr A string containing the path to the relevant directory.
 */
void load_file_info(size_t fileCount, DIR *workingDir, FileInfo *filenameList,
                    char const pathStr[PATH_MAX + 1]);

/**
 * @brief Print the file names of the files in the given FileInfo array.
 *
 * @param[in] filenameList A pointer to the array of FileInfo structs.
 * @param[in] fileCount The size of the array in terms of FileInfos.
 */
void print_file_info_list(FileInfo const *filenameList, size_t fileCount);

/**
 * @brief Comparator function for comparing FileInfos by fileSize.
 * @param[in] a The first FileInfo to compare
 * @param[in] b The second FileInfo to compare
 * @return An int based on which FileInfo has a larger fileSize.
 */
int cmp(void const *a, void const *b);
