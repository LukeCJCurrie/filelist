//
// Created by lukecjcurrie on 5/7/20.
//

#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef _WIN32
#include <direct.h>
#else

#include <bsd/string.h>
#include <sys/stat.h>

#endif

#include <FileList.h>

#define ERR_BUF_SIZE 256

void get_dir_path(int const argc, char const *const *const argv,
                  DIR **const workingDir, char pathStr[PATH_MAX + 1]) {
  if (argc == 1) {
    *workingDir = open_directory(".");

#ifndef _WIN32
    strlcpy(pathStr, ".", PATH_MAX + 1);
#else
    strcpy_s(pathStr, PATH_MAX + 1, ".");
#endif

  } else if (argc == 2) {
    *workingDir = open_directory(argv[1]);

#ifndef _WIN32
    strlcpy(pathStr, argv[1], PATH_MAX + 1);
#else
    strcpy_s(pathStr, PATH_MAX + 1, argv[1]);
#endif

  } else {
    fprintf(stderr, "Usage: %s [path-to-directory]\n", argv[0]);
    exit(EXIT_FAILURE);
  }
}

DIR *open_directory(char const pathStr[PATH_MAX + 1]) {
  DIR *workingDir = opendir(pathStr);

  if (NULL == workingDir) {
    fprintf(stderr, "Error opening current working directory.\n");
    exit(EXIT_FAILURE);
  }
  return workingDir;
}

size_t count_files(DIR *const workingDir) {
  dirent *entry = NULL;
  size_t fileCount = 0;
  while (NULL != (entry = readdir(workingDir))) {
    if (entry->d_type == DT_REG) {
      fileCount++;
    }
  }
  rewinddir(workingDir);
  return fileCount;
}

FileInfo *allocate_file_info_array(size_t const fileCount) {
  FileInfo *filenameList = (FileInfo *)calloc(fileCount, sizeof(FileInfo));
  if (NULL == filenameList) {
    fprintf(stderr, "Error allocating memory.\n");
    exit(EXIT_FAILURE);
  }
  return filenameList;
}

void load_file_info(size_t const fileCount, DIR *const workingDir,
                    FileInfo *const filenameList,
                    char const pathStr[PATH_MAX + 1]) {
  for (size_t i = 0; i < fileCount;) {
    const dirent *file = readdir(workingDir);
    if (NULL == file) {
      fprintf(stderr, "Prematurely reached end of directory.\n");
      rewinddir(workingDir);
      return;
    }
    if (file->d_type == DT_REG) {
      struct stat s;
      char filePath[PATH_MAX + 1];

#ifndef _WIN32
      strlcpy(filePath, pathStr, PATH_MAX + 1);
      strlcat(filePath, "/", PATH_MAX + 1);
      strlcat(filePath, file->d_name, PATH_MAX + 1);
      if (stat(filePath, &s) != -1) {
        strlcpy(filenameList[i].name, file->d_name, NAME_MAX + 1);

#else
      strcpy_s(filePath, PATH_MAX + 1, pathStr);
      strcat_s(filePath, PATH_MAX + 1, "/");
      strcat_s(filePath, PATH_MAX + 1, file->d_name);
      if (stat(filePath, &s) != -1) {
        strcpy_s(filenameList[i].name, NAME_MAX + 1, file->d_name);
#endif

        filenameList[i].fileSize = s.st_size;
        i++;
      } else {
        char errorMessage[ERR_BUF_SIZE] = {0};
#ifndef _WIN32
        strerror_r(errno, errorMessage, ERR_BUF_SIZE);
#else
        strerror_s(errorMessage, ERR_BUF_SIZE, errno);
#endif

        fprintf(stderr, "Error fetching file size for file %s: %s\n", filePath,
                errorMessage);
      }
    }
  }
  dirent *entry = NULL;
  while (NULL != (entry = readdir(workingDir))) {
    if (entry->d_type == DT_REG) {
      fprintf(stderr, "Files were missed: %s\n", entry->d_name);
      rewinddir(workingDir);
      return;
    }
  }
  rewinddir(workingDir);
}

void print_file_info_list(FileInfo const *const filenameList,
                          size_t const fileCount) {
  for (size_t i = 0; i < fileCount; i++) {
    printf("%s\n", filenameList[i].name);
  }
}

int cmp(const void *const a, const void *const b) {
  const FileInfo *ad = (const FileInfo *)a;
  const FileInfo *bd = (const FileInfo *)b;

  const off_t dif = bd->fileSize - ad->fileSize;

  if (dif > (off_t)0) {
    return 1;
  }
  if (dif < (off_t)0) {
    return -1;
  }
  return 0;
}
