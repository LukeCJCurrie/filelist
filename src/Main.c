//
// Created by lukecjcurrie on 5/7/20.
//
/**
 * command to build:
 * clang src/Main.c src/FileList.c -Wall -Wextra -Werror -pedantic -Iinclude/
 -lbsd -o FileList

 */

#include <stdio.h>
#include <stdlib.h>

#include <FileList.h>

/**
 * Entry point to program.
 * @return Program return code.
 */
int main(int const argc, char const *const *const argv) {

  DIR *workingDir = NULL;
  char pathStr[PATH_MAX + 1] = {0};
  get_dir_path(argc, argv, &workingDir, pathStr);

  size_t fileCount = count_files(workingDir);
  FileInfo *filenameList = allocate_file_info_array(fileCount);

  load_file_info(fileCount, workingDir, filenameList, pathStr);
  closedir(workingDir);

  qsort(filenameList, fileCount, sizeof(FileInfo), cmp);

  print_file_info_list(filenameList, fileCount);

  free(filenameList);
  return EXIT_SUCCESS;
}
